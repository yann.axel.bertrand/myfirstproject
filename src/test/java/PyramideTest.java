import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PyramideTest {

    static Pyramide pyramide = new Pyramide();

    @DisplayName("La liste doit être une liste de String non vide")
    @ParameterizedTest
    @ValueSource(ints = {1, 2, 7, 23})
    void pyramide(int taille) {
        List<String> result = pyramide.pyramide(taille);
        assertNotNull(result);
        for (String s : result){
            assertNotNull(s);
        }
    }

    @DisplayName("Elle doit retourner une liste de longueur (taille)*2-1")
    @ParameterizedTest
    @ValueSource(ints= {1,2,5,45})
    public void testBonneLongueur(int taille){
        List<String> result = pyramide.pyramide(taille);
        assertEquals(taille*2-1, result.size());
    }

    @ParameterizedTest
    @ValueSource(ints = {-1, -21, 0})
    @DisplayName("Elle doit retourner une exception si la taille est inférieur ou égale a 0")
    public void testExceptionSiZeroOuInferieur(int taille){
        assertThrows(PyramideBadArgumentException.class, () -> pyramide.pyramide(taille));
    }

    @ParameterizedTest
    @ValueSource(ints = {12,145,22})
    @DisplayName("Les string sont composés d'*")
    public void testChaineDEtoiles(int taille){
        List<String> result = pyramide.pyramide(taille);
        for(String s : result){
            assertTrue(s.matches("^[*]+$"));
        }
    }

    @ParameterizedTest
    @ValueSource(ints = {2,7,5})
    @DisplayName("Les string ont une longueur inférieure ou égale à la taille")
    public void testLongueurString(int taille){
        List<String> result = pyramide.pyramide(taille);
        assertEquals(taille, result.stream().mapToInt(String::length).max().orElse(0));
    }
}