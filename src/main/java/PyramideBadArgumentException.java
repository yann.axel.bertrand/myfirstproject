public class PyramideBadArgumentException extends RuntimeException {
    public PyramideBadArgumentException(String message) {
        super(message);
    }
}
